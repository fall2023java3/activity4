package linearalgebra;

public final class Vector3d {
    private final double x;
    private final double y;
    private final double z;
    
    public Vector3d(double a, double b, double c){
        this.x = a;
        this.y = b;
        this.z = c;
    }
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }
    public double magnitude(){
        double xyzSquaredAdded = x*x + y*y + z*z;
        return Math.sqrt(xyzSquaredAdded);
    }
    public double dotProduct(Vector3d vector){
        double vectorDot = ( vector.getX() * this.x ) + ( vector.getY() * this.y ) + ( vector.getZ() * this.z );
        return vectorDot;
    }
    public Vector3d add(Vector3d vector){
        double mixedX = vector.getX() + this.x;
        double mixedY = vector.getY() + this.y;
        double mixedZ = vector.getZ() + this.z;
        Vector3d newVector = new Vector3d(mixedX, mixedY, mixedZ); 
        return newVector;
    }

}
