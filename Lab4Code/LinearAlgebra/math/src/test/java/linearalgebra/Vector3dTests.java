package linearalgebra;
import static org.junit.Assert.*;
import org.junit.Test;

public class Vector3dTests {

    @Test
    public void testConstructorAndGetters() {
        // Create a Vector3d object
        Vector3d vector = new Vector3d(1.0, 2.0, 3.0);

        // Check if the getters return the correct values
        assertEquals(1.0, vector.getX(), 0.001);
        assertEquals(2.0, vector.getY(), 0.001);
        assertEquals(3.0, vector.getZ(), 0.001);
    }

    @Test
    public void testMagnitude() {
        // Create a Vector3d object
        Vector3d vector = new Vector3d(3.0, 4.0, 0.0);

        // Calculate the magnitude (should be 5.0)
        double magnitude = vector.magnitude();

        // Check if the magnitude is as expected
        assertEquals(5.0, magnitude, 0.001);
    }

    @Test
    public void testDotProduct() {
        // Create two Vector3d objects
        Vector3d vector1 = new Vector3d(1.0, 2.0, 3.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);

        // Calculate the dot product (should be 20.0)
        double dotProduct = vector1.dotProduct(vector2);

        // Check if the dot product is as expected
        assertEquals(20.0, dotProduct, 0.001);
    }

    @Test
    public void testAdd() {
        // Create two Vector3d objects
        Vector3d vector1 = new Vector3d(1.0, 2.0, 3.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);

        // Add the vectors
        Vector3d result = vector1.add(vector2);

        // Check if the result is as expected
        assertEquals(3.0, result.getX(), 0.001);
        assertEquals(5.0, result.getY(), 0.001);
        assertEquals(7.0, result.getZ(), 0.001);
    }
}